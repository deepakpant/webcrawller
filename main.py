import urllib
import re
import database
from urlparse import urlparse
from BeautifulSoup import BeautifulSoup,Comment #for windows use ' from bs4 import BeautifulSoup,Comment '
url="http://deepakpant.com.np"
urlStack=[]
urlCrawled=[]
keywords=[]
import fileinput
for line in fileinput.input("keywords.txt"):
    line=line.replace('\n','')
    keywords.append(line)
print keywords
urlStack.append(url) 
    
class Core:
    def extractTitle(self,soup):
        soup=BeautifulSoup(soup)
        title=soup.title
        text_parts = title.findAll(text=True)
        title = ''.join(text_parts)
        print title #text
        database.extractTitle(title)
    
    #remove scripts from documents
    def removeUnwantedTags(self,soup):
        soup=BeautifulSoup(soup)
        script=soup.findAll('script')
        [item.extract() for item in script]
        comments = soup.findAll(text=lambda text:isinstance(text, Comment))
        [comment.extract() for comment in comments]
        links=soup.findAll('link')
        [link.extract() for link in links]
        styles=soup.findAll('style')
        [style.extract for style in styles]
        return soup.prettify()

    def findMeta(self,soup):
        soup=BeautifulSoup(soup)
        meta=soup.findAll(content=True)
        metas=''
        for m in meta:
            metas=metas+' '+m['content']
        print metas
        database.findMeta(meta)

    

    def findImg(self,soup):
        soup=BeautifulSoup(soup)
        images=soup.findAll('img')
        for image in images:
            img_src=image['src']
            img_title=''
            print img_src
            database.findImg(img_src,img_title)

    def findLink(self,soup):
        global urlStack
        global urlCrawled
        soup=BeautifulSoup(soup)
        links=soup.findAll('a')
        for link in links:
            try:
                target=link['href'] 
            except:
                pass
            try:
                content=link.string
            except:
                pass
            if not target.startswith('http'):
                target="http://"+target      
            if target not in urlCrawled:
                if target not in urlStack:
                    url=urlparse(newUrl)
                    if target.startswith("http://"+url.netloc):
                        print "appending"+target
                        link=target #new line
                        urlStack.append(link) #urlStack.append(target)
                        database.findLink(link)
                    else:
                        #check url string is in keywords
                        for keyword in keywords:
                            if target.find(keyword) != -1:
                                print "appending "+target
                                link=target #new line
                                urlStack.append(link) #urlStack.append(target)
                                database.findLink(link)
                                
    def findTags(self,soup):
        soup=BeautifulSoup(soup)
        para=soup('p')
        h1=soup('h1')
        h2=soup('h2')
        h3=soup('h3')
        print h1+h2+h3+para
        database.findTags(para,h1,h2,h3)
            
ins=Core()


                           
                
                
    

while len(urlStack):
    newUrl=urlStack.pop()
    print "crawlling ",newUrl
    open=urllib.urlopen(newUrl)
    content=open.read()
    soup=BeautifulSoup(content)
    soup=soup.prettify()
    cleansoup=ins.removeUnwantedTags(soup)
    ins.extractTitle(cleansoup)
    ins.findMeta(cleansoup)
    ins.findTags(soup)
    ins.findImg(soup)
    ins.findLink(soup)
    urlCrawled.append(newUrl)
   
    


